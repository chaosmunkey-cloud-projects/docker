FROM python:3-slim

RUN apt update; \
    apt install -y awscli bat jq less
