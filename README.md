# Docker Images

This repo contains Dockerfiles for building container images to be used with CTFs or infrastructure projects. The aim is to create isolated environments which won't impact the local system by avoiding unnecessary and/or conflicting changes.
